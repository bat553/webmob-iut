import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
window.addEventListener('DOMContentLoaded', function() {
  console.log( ">>> Ready, after DOM loaded !" );
})
